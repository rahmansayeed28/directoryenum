
import requests 
import sys 

# Get input the dictioary file 
dictionary = input("Enter the dictionary file name with path: ")

# Get input the target URL
target= input("Enter the target url/IP: ")

print("Select the target type: 1) http(default) and 2) https")

input_url_type = input("Enter the target URL type (1 or 2):" )

if(input_url_type == 2):
    url_type = "https"
else:
    url_type = "http"


sub_dir_list = open(dictionary).read() 
directories = sub_dir_list.splitlines()

for dir in directories:
    dir_enum = f"{url_type}://{target}/{dir}.html" 
    # dir_enum = f"http://{sys.argv[1]}/{dir}.html" 
    r = requests.get(dir_enum)
    if r.status_code==404: 
        pass
    else:
        print("Valid directory:" ,dir_enum)